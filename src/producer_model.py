import math

class Producer:

    # Time-step simulation, e.g. 1 minutes per step --> 1.440 steps per day

    # Ausgangslage:
    # Einfamilienhaus: PV-Anlage mit **4KWp** = 4000 Watt (Theoretische Max-Leistung)

    # Im Durchschnitt kann eine PV-Anlage **pro kWp** etwa 2,7 kWh an einem sonnigen Tag erzeugen. (Quelle: https://www.solaranlagen-portal.com/photovoltaik/leistung#kennzahlen)
    # 2,7kWh * 4 * 1000/24 --> 450W

    # Bei Wolken: Wenn keine Sonne scheint, sinkt der Ertrag der Photovoltaikanlage auf etwa 0,15- bis 0,30 kW je kWp (Quelle: https://echtsolar.de/photovoltaik-ertrag-ohne-sonne/)
    # 0,3kW + 4 --> 120W


    def __init__(self, id, name, low_power, average_power, max_power, clouded_start = 14, clouded_duration = 2):
        self.id = id
        self.name = name
        self.low_power = low_power # Watt
        self.average_power = average_power # Watt
        self.max_power = max_power # Watt
        self.timestep = 0

        # values given in hours of day, e.g. 6.75 = 6:45 (AM)
        self.sunrise = 6
        self.sunset = 18
        self.clouded_start = clouded_start
        self.clouded_end = self.clouded_start + clouded_duration

    def get_power_output(self, time_of_day: float):
        if not self.sunrise < time_of_day < self.sunset:
            return 0.0

        x = (time_of_day - self.sunrise) / (self.sunset - self.sunrise)
        pf = math.sin(x*math.pi)

        if self.clouded_start < time_of_day < self.clouded_end:
            # a cloud reduces pwoer output to approx. 20%
            pf *= 0.2

        return self.max_power * pf

    def start_program_simple(self, sunny_start = 4, sunny_duration = 4, clouded_start = 12, clouded_duration = 2):
        sunny_end = sunny_start + sunny_duration
        clouded_end = clouded_start + clouded_duration
        if self.timestep >= sunny_start and self.timestep < sunny_end:
            return self.calculate_energy_output("sunny")
        elif self.timestep >= clouded_start and self.timestep < clouded_end:
            return self.calculate_energy_output("clouded")
        else:
            return self.calculate_energy_output()


    # Helper
    def calculate_energy_output(self, status = "average"): # Sunny, Average, Clouded
        if status == "sunny":
            self.timestep += 1
            return self.max_power # W
        elif status == "clouded":
            self.timestep += 1
            return self.low_power # W
        else: 
            self.timestep += 1
            return self.average_power # W
