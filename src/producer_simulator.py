import mosaik_api
import producer_model
from helpers.mosaik_helper import GenericSimulator, SimpleSimulator
from helpers.csv_lookup import CSVLookup

META = {
    'type': 'time-based',
    'models': {
        'ModelProducer': {
            'public': True,
            'params': ['low_power', 'average_power', 'max_power', 'clouded_start', 'clouded_duration'],
            'attrs': ['power_output'],
        },
        'LookupProducer': {
            'public': True,
            'params': ['max_power', 'start_timestamp'],
            'attrs': ['power_output'],
        },
    },
}

class ModelProducerSimulator(SimpleSimulator):
    def __init__(self, full_id, time_resolution, model, **kwargs):
        super().__init__(full_id, time_resolution, model)

        self.model_instance = producer_model.Producer(
            full_id, full_id,
            kwargs['low_power'],
            kwargs['average_power'],
            kwargs['max_power'],
            kwargs['clouded_start'],
            kwargs['clouded_duration']
        )

    def step(self, time, inputs, max_advance):
        # convert timestep to hours of day:
        hours_of_day = (time * self.time_resolution) / 3600
        self.output = self.model_instance.get_power_output(hours_of_day)
        return time + 1

    def get_data(self, attrs):
        data = {}
        for attr in attrs:
            if attr not in META['models']['ModelProducer']['attrs']:
                raise ValueError(f'Unknown output attribute: {attr}')
            if attr == 'power_output':
                data[attr] = self.output
        return data

class LookupProducerSimulator(SimpleSimulator):
    def __init__(self, full_id, time_resolution, model, samples, **kwargs):
        super().__init__(full_id, time_resolution, model)
        self.offset = int(kwargs['start_timestamp'])
        self.samples = samples
        self.peak_power_w = kwargs['max_power']

    def step(self, time, inputs, max_advance):
        self.time = self.offset + self.time_resolution * time
        return time + 1

    def get_data(self, attrs):
        data = {}
        for attr in attrs:
            if attr not in META['models']['LookupProducer']['attrs']:
                raise ValueError(f'Unknown output attribute: {attr}')
            if attr == 'power_output':
                data[attr] = self.samples.lookup(self.time) * self.peak_power_w
        return data

PV_SAMPLES_FILENAME = 'data/Actual_48.75_-122.45_2006_DPV_9MW_5_Min.csv'

class ProducerSimulator(GenericSimulator):
    def __init__(self):
        super().__init__(META, None, 'Prod_')
        self.database = CSVLookup(PV_SAMPLES_FILENAME, 1/9)

    def do_create(self, full_id, model, **kwargs):
        if model == 'ModelProducer':
            return ModelProducerSimulator(
                full_id, self.time_resolution, model, **kwargs)
        elif model == 'LookupProducer':
            return LookupProducerSimulator(
                full_id, self.time_resolution, model, self.database, **kwargs)

def main():
    return mosaik_api.start_simulation(ProducerSimulator())

if __name__ == '__main__':
    main()
