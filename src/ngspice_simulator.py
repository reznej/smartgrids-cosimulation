#!/usr/bin/env python3

import mosaik_api

from helpers.mosaik_helper import SimpleSimulator, FullSimulator
import ngspice_model

META = {
    'type': 'time-based',
    'models': {
        'SmartMeter': {
            'public': True,
            'params': [],
            'attrs': [
                'power_usage',
                'power_output',
                'power_grid_w',
                'power_total_w',
            ],
        },
    },
}


@FullSimulator(META)
class NgspiceSim(SimpleSimulator):
    """
    Simulate power balance in one household using Ngspice

    Input attributes:
    power_usage (INPUT):
        Every appliance should send it's current power usage (in W) to the
        controller

    power_output (INPUT):
        Every energy producer should send it's current power output (in W) to
        the controller

    power_grid_w (OUTPUT):
        Power that is used from the grid to satisfy the current total demand

    power_total_w (OUTPUT):
        Total amount of power that is used within the household (including
        self-produced power)
    """

    def __init__(self, full_id, time_resolution, model):
        super().__init__(full_id, time_resolution, model)
        self.data = {}

    def step(self, time, inputs, max_advance):
        self.time = time
        m = ngspice_model.Model()
        m.consumers = inputs.get('power_usage', {})
        m.producers = inputs.get('power_output', {})
        self.data = m.simulate()

        return time + 1

    def get_data(self, attrs):
        data = {}
        for attr in attrs:
            if attr not in META['models']['SmartMeter']['attrs']:
                raise ValueError(f'Unknown output attribute: {attr}')
            data[attr] = self.data[attr]
        return data

def main():
    return mosaik_api.start_simulation(ExampleSim())

if __name__ == '__main__':
    main()
