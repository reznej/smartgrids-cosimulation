"""
A simple data collector that prints all data when the simulation finishes.

"""
import collections
import json

import mosaik_api


META = {
    'type': 'event-based',
    'models': {
        'Monitor': {
            'public': True,
            'any_inputs': True,
            'params': [],
            'attrs': [],
        },
    },
}


class Collector(mosaik_api.Simulator):
    def __init__(self):
        super().__init__(META)
        self.eid = None
        self.data = collections.defaultdict(lambda:
                                            collections.defaultdict(dict))
        self.output_filename = None

    def init(self, sid, time_resolution, output_filename=None):
        self.time_resolution = time_resolution
        self.output_filename = output_filename
        return self.meta

    def create(self, num, model):
        if num > 1 or self.eid is not None:
            raise RuntimeError('Can only create one instance of Monitor.')

        self.eid = 'Monitor'
        return [{'eid': self.eid, 'type': model}]

    def step(self, time, inputs, max_advance):
        data = inputs.get(self.eid, {})
        for attr, values in data.items():
            for src, value in values.items():
                self.data[src][attr][time * self.time_resolution] = value

        return None

    def finalize(self):
        if self.output_filename is not None:
            with open(self.output_filename, 'w') as fout:
                json.dump(self.data, fout, separators=(',', ':'))


if __name__ == '__main__':
    mosaik_api.start_simulation(Collector())
