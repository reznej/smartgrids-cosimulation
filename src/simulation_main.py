#!/usr/bin/env python3

import mosaik

import importlib
import json
import os
from tempfile import NamedTemporaryFile

import helpers.plotting

SIM_CONFIG = {
    'NgspiceSim': {
        'python': 'ngspice_simulator:NgspiceSim',
    },
    'ApplianceSim': {
      'python': 'appliance_simulator:ApplianceSimulator',
    },
    'ControllerSim': {
      'python': 'controller_simulator:SmartControllerSimulator',
    },
    'EventSourceSim': {
      'python': 'eventsource_simulator:EventSourceSim',
    },
    'ProducerSim': {
      'python': 'producer_simulator:ProducerSimulator',
    },
    'ProsumerSim': {
      'python': 'prosumer_simulator:ProsumerSimulator',
    },
    'Collector': {
        'cmd': '%(python)s collector.py %(addr)s',
    }
}

RESOLUTION = 300  # simulate 5-minute steps
END = (24 * 3600) // RESOLUTION  # simulate one day

OUTPUT_DIR = './output/'

def load_scenario(scenario_name: str):
    scenario_pkg = importlib.import_module(f'scenarios.{scenario_name}')
    # trigger reload (for interactive use)
    importlib.reload(scenario_pkg)
    return scenario_pkg

def run_simulation(scenario_name: str, scenario_pkg, smart: bool):
    """
    Run the given scenario

    Returns the filename where the simulation results are stored.
    """
    world = mosaik.World(SIM_CONFIG, time_resolution=RESOLUTION)

    os.makedirs(OUTPUT_DIR, exist_ok=True)
    with NamedTemporaryFile(dir=OUTPUT_DIR, delete=False) as tmpf:
        collector = world.start('Collector', output_filename=tmpf.name)
        monitor = collector.Monitor()

        scenario_pkg.setUp(world, monitor, smart)

        world.run(until=END)

        output_filename = os.path.join(OUTPUT_DIR,
                                       f'{scenario_name}_data.json')
        os.rename(tmpf.name, output_filename)

    return output_filename

def run_get_results(scenario_name: str, scenario_pkg, smart: bool):
    data_filename = run_simulation(scenario_name, scenario_pkg, smart)
    with open(data_filename, 'rb') as f:
        data = json.load(f)
    return data


ALL_SCENARIOS = [
    'basic_simple',
    'basic_smart',
    'complex_simple',
    'complex_smart',
]

if __name__ == '__main__':
    for scenario_name in ALL_SCENARIOS:
        scenario_base = scenario_name[:scenario_name.find('_')]
        smart = scenario_name.endswith('_smart')

        scenario_pkg = load_scenario(scenario_base)
        data = run_get_results(scenario_name, scenario_pkg, smart)
        np_data = helpers.plotting.gather_results(data)
        fig = scenario_pkg.plot(np_data)
        fig.savefig(os.path.join(OUTPUT_DIR, f'{scenario_name}_plot.pdf'))
        summary = scenario_pkg.summary(np_data).lstrip('\n')
        sumamry_filename = os.path.join(OUTPUT_DIR, f'{scenario_name}_summary.txt')
        with open(sumamry_filename, 'w', encoding='utf-8') as f:
            f.write(summary)
        print(summary)
