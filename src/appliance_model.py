from typing import List

class Appliance:

    def __init__(self, id, name, idle_power, active_power,
                 program_duration_sec, behaviour):
        self.model = 'Appliance'
        self.id = id
        self.name = name
        self.idle_power = idle_power # Watt
        self.active_power = active_power # Watt
        if behaviour not in ['wm_simple', 'wm_smart']:
            raise ValueError(f'Unknown behaviour for Appliance: {behaviour}')
        self.behaviour = behaviour

        self.ready = False
        self.program_duration = program_duration_sec
        self.remaining_time = 0

    def prepare_program(self):
        if not self.is_active():
            self.ready = True
        else:
            raise ValueError("is already running")

    def start_program(self):
        if self.ready:
            self.remaining_time = self.program_duration
            self.ready = False
        else:
            raise ValueError("can only run when ready")

    def is_active(self):
        return self.remaining_time > 0

    def step(self, timestep_seconds: int, allocated_extra_power: float):
        if self.is_active():
            self.remaining_time -= timestep_seconds

        if self.behaviour == 'wm_smart':
            if self.ready:
                if allocated_extra_power >= (self.active_power - self.idle_power):
                    self.start_program()
            # TODO: countdown max_ready_wait timer and start when 0
        elif self.behaviour == 'wm_simple':
            if self.ready:
                self.start_program()

    def get_optional_extra_power_usage(self) -> List[float]:
        """
        Return a list of power modes, that the appliance COULD use (in
        addition to current power usage).
        """
        if self.behaviour == 'wm_smart':
            if self.ready:
                # appliance is ready but not running yet, it COULD start at any
                # time:
                return [self.active_power - self.idle_power]
        return []

    # Helper
    def calculate_power_usage(self):
        if self.is_active():
            return self.active_power
        else: 
            return self.idle_power


# Instances

# Idea: Define power output tresholds for typical appliances like washer, refrigerator etc. e.g. > 1500W and < 2500W. Randomize value on instance init.

# def main():
#   # Washer - Idle-Power: 3W, Active-Power: 2000W, wash cycle duration: 2h (8 time-steps)
#   washer = Appliance(1, "Washer", False, 3, 2000)
#   # washer.start_program(8)
#   washer_daily = 0
#   for step in range(1, washer.max_step+1):
#       if (step >= 30 and step < 34) or (step >= 60 and step < 63):
#           washer_daily += washer.start_program()
#           washer.set_last_active(step)
#       washer_daily += washer.calculate_energy_consumption()
#   print(f"Washer daily consumption: {round(washer_daily, 3)} kWh")

#   # Refrigerator - Constant Active-Power: 180W all day
#   refrigerator = Appliance(2, "Refrigerator", True, 0, 180)
#   refrigerator_daily = 0
#   for step in range(1, refrigerator.max_step+1):
#       # print(step, refrigerator_daily)
#       refrigerator_daily += refrigerator.calculate_energy_consumption()
#       refrigerator.set_last_active(step)
#   print(f"Refrigerator daily consumption: {round(refrigerator_daily, 3)} kWh")

# if __name__ == '__main__':
#   main()
