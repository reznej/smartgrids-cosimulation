import mosaik_api
import appliance_model
import appliance_thermal_model
from helpers.mosaik_helper import FullSimulator, SimpleSimulator

META = {
    'type': 'time-based',
    'models': {
        'Appliance': {
            'public': True,
            'params': ['idle_power', 'active_power', 'behaviour', 'program_duration'],
            'attrs': [
                'opt_power_modes',
                'extra_power_allocation',
                'power_usage',
                'ready',
            ],
        },
        'ThermalAppliance': {
            'public': True,
            'params': ['idle_power', 'active_power', 'behaviour', 'loss_idle', 'loss_active', 'heat_factor', 'target_temp'],
            'attrs': [
                'env_temp',
                'inner_temp',
                'opt_power_modes',
                'extra_power_allocation',
                'power_usage',
                'ready',
            ],
        },
    },
}

@FullSimulator(META, default_eid_prefix='App_')
class ApplianceSimulator(SimpleSimulator):
    def __init__(self, full_id, time_resolution, model, **kwargs):
        super().__init__(full_id, time_resolution, model)

        if model == 'Appliance':
            self.model_instance = appliance_model.Appliance(
                full_id, full_id,
                kwargs['idle_power'],
                kwargs['active_power'],
                kwargs['program_duration'],
                kwargs['behaviour'],
            )
        elif model == 'ThermalAppliance':
            self.model_instance = appliance_thermal_model.ThermalAppliance(
                full_id, full_id, **kwargs)

    def step(self, time, inputs, max_advance):
        if 'ready' in inputs:
            self.model_instance.prepare_program()
        extra_power = self.find_power_budget(inputs.get('extra_power_allocation'))
        self.model_instance.step((time - self.time) * self.time_resolution,
                            extra_power)

        self.time = time
        return time + 1

    def find_power_budget(self, attrs) -> float:
        if attrs is None:
            return 0.0

        for src, allocations in attrs.items():
            if self.full_id in allocations:
                return allocations[self.full_id]
        return 0.0

    def get_data(self, attrs):
        data = {}
        for attr in attrs:
            model = self.model_instance.model
            if attr not in META['models'][model]['attrs']:
                raise ValueError(f'Unknown output attribute: {attr}')
            if attr == 'power_usage':
                data[attr] = self.model_instance.calculate_power_usage()
            elif attr == 'opt_power_modes':
                data[attr] = self.model_instance.get_optional_extra_power_usage()
            else:
                data[attr] = getattr(self.model_instance, attr)

        return data

def main():
    return mosaik_api.start_simulation(ApplianceSimulator())

if __name__ == '__main__':
    main()
