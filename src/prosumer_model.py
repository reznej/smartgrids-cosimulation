from typing import List, Tuple

import appliance_model
import producer_model

class Prosumer(appliance_model.Appliance, producer_model.Producer):
    """
    A very simple model of a "smart" battery, which can switch between charging
    or supplying current back tot he grid.
    """

    def __init__(self, id, name, battery_charge, battery_power, battery_capacity):
        self.id = id
        self.name = name
        self.battery_charge = battery_charge # kWh
        self.battery_power = battery_power # W (Tesla 3: 11.000W) https://ev-database.de/pkw/1322/Tesla-Model-3-Performance
        self.battery_capacity = battery_capacity #kWh (Tesla 3: 82kWh) https://ev-database.de/pkw/1322/Tesla-Model-3-Performance
        self.timestep = 0

        self.charging_efficiency = 0.9
        self.supply_efficiency = 0.9

        # idle mode is always at position 0:
        self.power_modes = [0]
        self.mode = 0

        GRANULARITY = 10
        # assume the battery can regulate its charging speed in 10 discrete
        # steps:
        for i in range(GRANULARITY + 1):
            self.power_modes.append((i * self.battery_power) // GRANULARITY)

        # assume the battery can discharge and charge at the same rate:
        for i in range(-GRANULARITY, 0):
            self.power_modes.append((i * self.battery_power) // GRANULARITY)

    def battery_charge_level(self):
        return self.battery_charge / self.battery_capacity

    def get_optional_extra_power_usage(self) -> List[float]:
        now = self.calculate_power_usage()

        # we need to return the difference to the current power usage:
        return [m - now for _, m in self.eligible_power_modes()]

    def eligible_power_modes(self) -> List[Tuple[int, float]]:
        result = []

        for i, power in enumerate(self.power_modes):
            # do not discharge if almost empty
            if self.battery_charge_level() < 0.2 and power < 0:
                continue

            # can not charge when full
            if self.battery_charge_level() > 0.99 and power > 0:
                continue

            # can only charge at half speed when almost full
            if self.battery_charge_level() > 0.8 and power > 0.5 * self.battery_power:
                continue

            # power mode has passed all checks
            result.append((i, power))

        return result

    def switch_power_mode(self, new_mode: int):
        if self.mode == new_mode:
            return

        new_p = self.power_modes[new_mode]
        print(f"Prosumer {self.id} switches power mode from {self.mode} -> {new_mode} ({new_p/1000} kW)")
        self.mode = new_mode

    def step(self, timestep_seconds: int, allocated_extra_power: float):
        target_power = self.calculate_power_usage() + allocated_extra_power
        next_mode = self.find_power_mode(target_power)

        self.switch_power_mode(next_mode)

        charge_delta_joule = timestep_seconds * self.calculate_power_usage()
        charge_delta_kwh = charge_delta_joule / 3.6e6

        if charge_delta_kwh > 0:
            # added battery charge is reduced by given efficiency
            self.battery_charge += charge_delta_kwh * self.charging_efficiency

        if charge_delta_kwh < 0:
            # reduced supplying efficiency means that we need to take more
            # charge out of the battery then is supplied to the grid
            self.battery_charge += charge_delta_kwh / self.supply_efficiency

    def find_power_mode(self, target_power: float) -> int:
        """
        Find a power mode that is within 10W of the desired power usage
        """
        for i, m in self.eligible_power_modes():
            if round(target_power/10) == round(m/10):
                return i

        # no suitable mode found, fallback to idle mode:
        return 0

    # Helper
    def calculate_power_usage(self):
        return self.power_modes[self.mode]
