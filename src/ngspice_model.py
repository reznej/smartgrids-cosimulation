from PySpice.Spice.Netlist import Circuit
from PySpice.Unit import as_A, u_V, u_A, u_Ohm, u_MOhm, u_W, U_W
from PySpice.Unit.Unit import UnitValue

class CircuitBuilder:
    def __init__(self, name):
        self.next_net = 1
        self.net_names = {
            'ground': 0,
        }
        self.circ = Circuit(name)

    def named_net(self, name: str) -> int:
        if name not in self.net_names:
            self.net_names[name] = self.next_net
            self.next_net += 1

        return self.net_names[name]

    def __getitem__(self, idx: str) -> int:
        return self.named_net(idx)

    def circuit(self) -> Circuit:
        return self.circ

class Model:

    def __init__(self, voltage=230):
        self.voltage = voltage
        self.consumers = {}
        self.producers = {}

    def simulate(self):
        circuit = self.prepare_circuit()

        sim = circuit.simulator()
        op = sim.operating_point()
        return self.extract_data(op)

    def extract_data(self, op):
        power_usage = {}

        # for name in self.consumers:
            # power_usage[name] = op.branches

        total_power_usage = self._power_from_current(
            op.branches['vmestotal'][0])
        grid_power_usage = self._power_from_current(
            op.branches['vmesgrid'][0])

        return {
            'power_grid_w': round(grid_power_usage.convert(U_W).value),
            'power_total_w': round(total_power_usage.convert(U_W).value),
        }

    def _power_from_current(self, current: UnitValue) -> UnitValue:
        return as_A(current) * u_V(self.voltage)

    def prepare_circuit(self) -> Circuit:
        b = CircuitBuilder('simulation')
        c = b.circuit()

        c.VoltageSource('ref', b['ref'], b['ground'], u_V(self.voltage))
        c.VoltageSource('MEStotal', b['main'], b['consumers'], 0@u_V)
        c.VoltageSource('MESgrid', b['grid'], b['main'], 0@u_V)

        c.VoltageControlledCurrentSource(
            'grid',
            b['grid'], b['ground'],
            b['main'], b['ref'],
            1000000
        )
        have_consumers = False
        for name, req_power in self.consumers.items():
            if req_power == 0:
                continue
            resistance = u_Ohm(self.voltage ** 2 / req_power)
            c.R(f'cons_{name}', b['consumers'], b['ground'], resistance)
            have_consumers = True

        if not have_consumers:
            # add a high resistance as a base load, to avoid the pathological
            # case of a circuit without any load
            c.R('mx', b['consumers'], b['ground'], 1000@u_MOhm)

        for name, prod_power in self.producers.items():
            prod_current = u_W(prod_power) / u_V(self.voltage)
            c.CurrentSource(f'prod_{name}', b['ground'], b['main'],
                            prod_current)

        return c
