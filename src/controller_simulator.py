#!/usr/bin/env python3

import mosaik_api

from helpers.mosaik_helper import SimpleSimulator, FullSimulator
import ngspice_model

META = {
    'type': 'time-based',
    'models': {
        'SmartController': {
            'public': True,
            'params': [],
            'attrs': [
                'power_grid_w',
                'extra_power_requests',
                'extra_power_allocation',
            ],
        },
    },
}


@FullSimulator(META)
class SmartControllerSimulator(SimpleSimulator):
    """
    Intelligently distribute the available power within a household
    """

    def __init__(self, full_id, time_resolution, model):
        super().__init__(full_id, time_resolution, model)
        self.data = {}

    def step(self, time, inputs, max_advance):
        power_grid = sum(inputs['power_grid_w'].values())

        extra = self.distribute_extra_power(
            power_grid,
            inputs.get('extra_power_requests', {})
        )
        supply = self.acquire_extra_power(
            power_grid,
            inputs.get('extra_power_requests', {})
        )
        extra.update(supply)
        self.data['extra_power_allocation'] = extra

        return time + 1

    def distribute_extra_power(self, power_grid, requests):
        """
        When more power is available than is currently used, it makes sense to
        try and distribute it within the household, instead of feeding it into
        the grid.
        """
        if power_grid >= 0:
            # no extra power to distribute
            return {}

        # Grid power is negative, which means we are currently feeding power
        # into the grid. Try to distribute it among interested consumers.
        remaining_budget = -power_grid
        allocations = {}

        # sort candidates by name, so that the priority is predictable
        candidates = sorted(requests.items())
        for candidate, modes in candidates:
            # sort in reverse, so that we try high-power modes first:
            for req_power in sorted(modes, reverse=True):
                if req_power > 0 and req_power <= remaining_budget:
                    remaining_budget -= req_power
                    allocations[candidate] = req_power
                    break

        return allocations

    def acquire_extra_power(self, power_grid, requests):
        """
        Try to reduce current power usage, this can be done by
          a) Switching devices to a lower power level
          b) Asking a "Prosumer" device to supply power
        From the perspective of the SmartController, both alternatives are
        treated the same (i.e. a device promises to reduce overall power usage).
        """
        if power_grid <= 0:
            # no extra power required
            return {}

        remaining_demand = power_grid
        allocations = {}

        # sort candidates by name, so that the priority is predictable
        candidates = sorted(requests.items())
        for candidate, modes in candidates:
            # sort, so that we try high-power modes first, since we're looking
            # for negative values (supply), `reverse` is not required
            for delta_power in sorted(modes):
                # invert the sign to make following code simpler
                offered_power = -delta_power
                # print(f"check: {candidate}, {delta_power} vs {remaining_demand}")
                if offered_power > 0 and offered_power <= remaining_demand:
                    remaining_demand -= offered_power
                    allocations[candidate] = delta_power
                    break

        return allocations

    def get_data(self, attrs):
        data = {}
        for attr in attrs:
            if attr not in ['extra_power_allocation']:
                raise ValueError(f'Unknown output attribute: {attr}')
            data[attr] = self.data[attr]
        return data

def main():
    return mosaik_api.start_simulation(SmartControllerSimulator())

if __name__ == '__main__':
    main()
